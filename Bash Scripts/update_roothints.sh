#!/bin/bash
# 🌟 Gestionar fichero de sugerencias de raíz de Unbound /etc/unbound/root.hints 🌟
# 📂⨍() /usr/local/bin/update_roothints.sh 👑
# 🌐 https://wiki.archlinux.org/title/unbound#Roothints_systemd_timer
# 🗑️ /etc/systemd/system/roothints.path
#   /etc/systemd/system/roothints.timer
# 🔧 /etc/systemd/system/roothints.service
#  /usr/local/bin/notify_roothints.sh

# Descargar fichero root.hints.
download_file(){
    local -r file="$1" url="$2"
    curl --retry 3 --output "$file" "$url"
}

# Notificar usando funsión notify_action del script externo de usuario /usr/local/bin/notify_roothints.sh.
notify() {
    local -r urgency="$1" icon="${2:-dialog-information-symbolic}" tittle_notification="${3:-Unbound 🌐}" head_notification="$4"
    notify_action "$urgency" "$icon" "$tittle_notification" "$head_notification" 
}

# Reiniciar servicio de unbound.service.
restart_unbound() {
    systemctl restart unbound.service
}

# Actualiza o crea el fichero de sugerencias de raíz.
update_file() {
    local -r update_file="$1" outdated_file="$2"
    local msg_file='' urgency='' icon='' tittle_notification='' head_notification='Actualizado'

    if [[ ! "$update_file" =~  ^.*\.new$ ]] && [[ ! "$outdated_file" =~  ^.*\.new$ ]]; then
        head_notification+=' en <b>local</b> ,'
        if [[ "$outdated_file" =~  ^.*\.back$ ]]; then
       	    msg_file=' backup del'
        fi
    fi

    if cp "$update_file" "$outdated_file"; then
        [ "$outdated_file" == 'root.hints' ] && restart_unbound
        head_notification+="<b>\"$outdated_file\"</b>,$msg_file fichero de sugerencias de raíz!"
        urgency='normal'; icon='selection-mode-symbolic'
    else
        head_notification="Error al actualizar$msg_file fichero de sugerencias de raíz, requiere intervención manual."
        urgency='critical'; icon='process-stop-symbolic'
    fi
    echo ">> $head_notification" && \
    notify "$urgency" "$icon" "$tittle_notification" "$head_notification"
}

# Actualizar ambos ficheros de sugerencias de raíz (original y backup).
update_both_files() {
    local -r new_file="$1" file="$2" back_file="$3"
    local new_file_content='' manual_intervention=', se requiere intervención manual.'

    if [ -f "$new_file" ]; then
        new_file_content="$(cat "$new_file")"
        if [[ "$new_file_content" =~ INTERNIC ]]; then
            if tee "$file" "$back_file" < "$new_file" && restart_unbound; then
    	        notify 'normal' 'selection-mode-symbolic' '' "Actualizados ficheros <b>$file</b> y <b>"$back_file"</b>."
            else
    	        notify 'critical' 'process-stop-symbolic' '' "Error al actualizar fichero de sugerencias de raíz y backup$manual_intervention."
            fi
        else
    	    notify 'critical' 'process-stop-symbolic' '' "Contenido de fichero <b>\"${new_file##*/}\"</b> errado$manual_intervention."
        fi
    else
        notify 'critical' 'process-stop-symbolic' '' "No es posible obtener <b>\"${new_file##*/}\"</b>, revise su conexión a Internet."
    fi
}

# Obtener versión del fichero de sugerencias de raíz.
get_version_file() {
    local -r file="$1"
    local -r version_file="$(sed -En 's/.+related version of root zone: ?+(.+)/\1/p' "$file")"
    echo "$version_file"
}

# Validar versión de los ficheros de raíz.
validate_version_file() {
    local -r new_file="$1" file="$2"
    local -r version_new_file="$(get_version_file "$new_file")" version_file="$(get_version_file "$file")"

    if [ "$version_new_file" != "$version_file" ]; then
        if (( $version_new_file > $version_file )); then
            update_file "$new_file" "$file"
        elif (( $version_file > $version_new_file )); then
            update_file "$file" "$new_file"
        else
            echo '>> Versión de ficheros raíz errada!'
        fi
    else
        echo ">> Versión de \"$new_file=$version_new_file\", es igual a versión de \"$file=$version_file\""
    fi
}

# Valida si fichero de sugerencias de raíz existe y es distinto al comparado.
validate_file_difference() {
    local -r new_file="$1" file="$2"

    if [ ! -f "$file" ]; then
        update_file "$new_file" "$file"
    elif ! cmp -s "$new_file" "$file"; then
        validate_version_file "$new_file" "$file"
    else
        echo ">> Contenido de \"$new_file\" y \"$file\" es igual"
    fi
}

# Valida si nuevo fichero de sugerencias de raíz existe y su contenido es válido.
validate_new_file() {
    local -r new_file="$1" existing_file="$2" non_existing_file="$3" manual_intervention=', se requiere intervención manual.'
    local new_file_content=''

    if [ -f "$new_file" ]; then
        new_file_content="$(cat "$new_file")"
        if [[ "$new_file_content" =~ INTERNIC ]]; then
            echo ">> Contenido de \"$new_file\" es válido."
            validate_file_difference "$new_file" "$existing_file"
        else
    	    notify 'critical' 'process-stop-symbolic' '' "Contenido de fichero <b>\"${new_file##*/}\"</b> errado$manual_intervention."
        fi
    else
        notify 'critical' 'process-stop-symbolic' '' "No es posible obtener <b>\"${new_file##*/}\"</b>, revise su conexión a Internet."
    fi
    validate_file_difference "$existing_file" "$non_existing_file"
}

# Validar ficheros "root.hints" y "root.hints.back".
validate_files(){
    local -r unbound_path="$1" file_name="$2" new_file=$3
    local -r file="$unbound_path/$file_name" back_file="$unbound_path/$file_name.back"

    if [ -f "$file" ] && [ -f "$back_file" ]; then
        echo '>> Ambos ficheros de raíz existen'
        validate_new_file "$new_file" "$file" "$back_file"
    elif [ ! -f "$file" ] && [ ! -f "$back_file" ]; then
        echo '>> Ningún fichero de raíz existe'
        update_both_files "$new_file" "$file" "$back_file"
    elif [ -f "$file" ] && [ ! -f "$back_file" ]; then
        echo ">> \"$back_file\" no existe."
        validate_new_file "$new_file" "$file" "$back_file"
    elif [ ! -f "$file" ] && [ -f "$back_file" ]; then
        echo ">> \"$file\" no existe."
        validate_new_file "$new_file" "$back_file" "$file"
    fi
    rm -rf "$temp_dir"
}

# Hacer ping a una URL.
ping_url() {
    local -r url="$1" attempts=$2
    local connected='false'

    for i in {1..$attempts}; do
        if ping -c2 "$url" &>/dev/null; then
            connected='true'
            break
        else
            sleep .5
        fi
    done
    echo "$connected"
}

# Valida si hay conexión a Internet.
validate_internet_connection() {
    local -r unbound_path='/etc/unbound' file_name='root.hints' temp_dir=$(mktemp -d -t roothints.XXXXXX) url_arch='archlinux.org'
    local -r new_file="$temp_dir/$file_name.new" connected="$(ping_url "$url_arch" 2)" url_roothints='https://www.internic.net/domain/named.cache'

    if [ "$connected" == 'true' ]; then
        download_file "$new_file" "$url_roothints" || restart_unbound && download_file "$new_file" "$url_roothints" 
    elif [ "$connected" == 'false' ]; then
        notify 'critical' 'network-wireless-disabled-symbolic' '' "Actualización en red de <b>$file_name</b> requiere conexión a Internet." && \
		sleep 3
    fi
    validate_files "$unbound_path" "$file_name" "$new_file"
}

# Función principal: Valida si el usuario tiene permisos de superusuario.
main() {
    # Importo módulo de notificaciones.
    source /usr/local/bin/notify_roothints.sh
    if [ "$EUID" == 0 ]; then
        validate_internet_connection
    else
        notify "normal" '' '' "Necesita permisos de <i>superusuario</i> para ejecutar el script <b>${0##*/}</b>." 
    fi
}

main
