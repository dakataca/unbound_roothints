#!/bin/bash
# 🌟 Notificar acciones sobre el fichero de sugerencias de raíz de Unbound /etc/unbound/root.hints 🌟 
# 📂 /usr/local/bin/notify_roothints.sh 👑
# 🌐 https://wiki.archlinux.org/title/unbound#Roothints_systemd_timer
# 🗑️ /etc/systemd/system/roothints.path
#   /etc/systemd/system/roothints.timer
# 🔧 /etc/systemd/system/roothints.service
# ⨍() /usr/local/bin/update_roothints.sh

# Obtener usuario humano (no root) de la sesión actual.
get_user_current_session() {
    local -r session_user=$(loginctl | sed -nE "s/.+[0-9]{4} (.+) seat.+/\1/p" 2>/dev/null)
    echo "$session_user"
}

# Imprime notificación usando echo.
print_notification(){
    local -r title_notification="$1" head_notification="$2"
    echo -e ">> $tittle_notification\n> $head_notification"
}

# Notificar acción realizada usando libnotify (notify-send).
notify_action(){
    local -r urgency="$1" icon="$2" tittle_notification="$3" head_notification="$4" user=$(get_user_current_session) 
    local id_user='' dsba=''
 
    if [ -n $user ]; then
        id_user=$(id -u $user)
        dsba="${DBUS_SESSION_BUS_ADDRESS:-unix:path=/run/user/$id_user/bus}"
        if ! sudo -u $user DBUS_SESSION_BUS_ADDRESS="$dsba" \
		notify-send  --app-name="Unbound" --urgency "$urgency" -t 11000 --icon "$icon" "$tittle_notification" "$head_notification" 2>/dev/null; then
            echo ">> Error, notify-send requiere de una sesión gráfica activa."
	fi
    else
        echo '>> Usuario normal no ha iniciado sesión.' 
    fi
    print_notification "$tittle_notification" "$head_notification"
}
