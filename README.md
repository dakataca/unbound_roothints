# unbound_roothints
Es un conjunto de scripts `bash` y unidades `systemd` encargados de gestionar el fichero de sugerencias de raíz de Unbound `/etc/unbound/root.hints`.

## Explicación

### Bash scripts
#### `update_roothints.sh` 
- Valida el fichero `/etc/unbound/root.hints` y su copia de seguridad `/etc/unbound/root.hints.back`. Ambos ficheros se traen desde https://www.internic.net/domain/named.cache, como lo recomienda la [wiki de Archlinux](https://wiki.archlinux.org/title/unbound#Root_hints).
- Garantiza el reinicio del servicio `unbound.service` cuando el fichero principal sufre cambios.
- Verifica la versión de los ficheros y los reemplaza cuando el nuevo fichero `root.hints`tiene una versión más nueva.
- Si no hay conexión a internet valida en local `/etc/unbound/root.hints` y su copia de seguridad `/etc/unbound/root.hints.back`, si alguno que los dos no existe lo crea.

#### `notify_roothints.sh`
Envía notificaciones usando el comando `notify-send` cuando:
- No hay conexión a Internet.
- Los ficheros `/etc/unbound/root.hints` y/o `/etc/unbound/root.hints.back` fueron modificados.
- Se requiera intervención manual.

### Unidades systemd
#### `roothints.service`
Servicio que ejecuta el script `update_roothints.sh`, el cual a su vez envía notificaciones a través de `notify_roothints.sh`.

#### `roothints.timer`
Temporizador que ejecuta el servicio `roothints.service` semanalmente.

#### `roothints.path`
Unidad systemd basada en ruta que monitorea los ficheros `/etc/unbound/root.hints` y `/etc/unbound/root.hints.back`, cuando alguno de éstos o ambos sean eliminados manualmente, automáticamente se activa ésta unidad y ejecuta el servicio `roothints.service` para restablecer los ficheros y las consultas DNS con el servidor Unbound.

## Instalación

### Archlinux
Usando un ayudador como paru o yay.
```sh
paru -S unbound-roothints
```

### Otras distros
En proceso...